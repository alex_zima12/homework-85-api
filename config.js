const path = require("path");

const rootPath = __dirname;

let dbName = "musicApi";

if (process.env.NODE_ENV === "test") {
    dbName = "musicApi";
}

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public/uploads"),
    db: {
        name: dbName,
        url: "mongodb://localhost"
    },

    fb: {
        appId: "676780759870221",
        appSecret: "8ae4ea77f1f37e882eaa2da502bca25a",
        // process.env.FB_SECRET
    }
};