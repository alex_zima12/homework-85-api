const express = require('express');
const router = express.Router();
const TrackHistory = require("../models/TrackHistory");
const auth = require('../middleware/auth');

const createRouter = () => {

    router.get("/", auth, async (req, res) => {
        try {
            const trackHistory = await TrackHistory.find({user: req.user._id}).sort ({datetime: -1}).populate({path: 'track', populate: {path: 'album',populate: {path: 'artist'}}});
            res.send(trackHistory);
        } catch (e) {
            return res.status(400).send({error: 'Failure'});
        }
    });

    router.post("/",auth, async (req, res) => {
        req.body.user = req.user._id;
        const trackHistoryData = new TrackHistory(req.body);
        try {
            await trackHistoryData.save();
            res.send(trackHistoryData);
        } catch (e) {
            return res.status(400).send({error: 'Failure'});
        }
    });
    return router;
};

module.exports = createRouter;