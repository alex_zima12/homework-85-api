const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: "Album",
        require: true
    },
    duration: {
        type: String
    },
    trackNum: {
        type: Number,
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Track = mongoose.model("Track", TrackSchema);

module.exports = Track;