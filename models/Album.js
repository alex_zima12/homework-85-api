const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    artist: {
        type: Schema.Types.ObjectId,
        ref: "Artist",
        require: true
    },
    image: String,
    date: {
        type: String,
        default: Date.now
    },
    year: {
        type: Number,
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Album = mongoose.model("Album", AlbumSchema);

module.exports = Album;