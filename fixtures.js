const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");

const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("albums");
        await db.dropCollection("artists");
        await db.dropCollection("tracks");
        await db.dropCollection("users");
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }

    const [user, admin] = await User.create({
        username: "user",
        role: "user",
        password: "1@345qWert",
        displayName: "display user",
        token: nanoid()
    },
        {
        username: "admin",
        role: "admin",
        password: "1@345qWert",
        displayName: "display admin",
        token: nanoid()
    });

    const [beyonce, ArianaGrande] = await Artist.create({
        title: "beyonce",
        user: admin._id,
        image: "eGvyq6I0S_FlN53OtYLUJ.png",
        information: "Beyoncé, in full Beyoncé Giselle Knowles, (born September 4, 1981, Houston, Texas, U.S.), American singer-songwriter and actress who achieved fame in the late 1990s as the lead singer of the R&B group Destiny’s Child and then launched a hugely successful solo career.",
        published: true
    }, {
        title: "ArianaGrande",
        user: user._id,
        image: "AmusicApi.p78H6xkzaYvD1IdppThE.jpg",
        information: "Grande was born on June 26, 1993, in Boca Raton, Florida, to Edward Butera, a graphic designer, and Joan Grande, a businesswoman.",
        published: true
    });

    const [Lemonade, Positions] = await Album.create({
        title: "Positions",
        user: admin._id,
        artist: beyonce._id,
        image: "eGvyq6I0S_FlN53OtYLUJ.png",
        year: '2013',
        published:true
    }, {
        title: "Lemonade",
        user: user._id,
        artist: ArianaGrande._id,
        image: "Ap78H6xkzaYvD1IdppThE.jpg",
        year: '2016',
        published:false
    });

    await Track.create({
            title: "track1",
            user: user._id,
            album: Lemonade._id,
            duration: "3:15",
            trackNum: 1,
            published:false
        }, {
            title: "track2",
            user: user._id,
            album: Lemonade._id,
            duration: "3:15",
            trackNum: 2,
            published:false
        },
        {
            title: "track3",
            user: user._id,
            album: Lemonade._id,
            duration: "3:15",
            trackNum: 3,
            published:false
        },
        {
            title: "track1",
            user: user._id,
            album: Positions.id,
            duration: "3:15",
            trackNum: 1,
            published: true
        },
        {
            title: "track2",
            user: user._id,
            album: Positions.id,
            duration: "3:15",
            trackNum: 2,
            published: true
        },
        {
            title: "track3",
            user: user._id,
            album: Positions.id,
            duration: "3:15",
            trackNum: 3,
            published:false
        });



    db.close();
});