const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/",auth, async (req, res) => {
        const user = req.user;
        let album;
        if (user.role === "user") {
            try {
                if (req.query.artist) {
                    album = await Album.find({"artist": req.query.artist},{"published": true}).sort({year: 1}).populate('artist');
                } else {
                    album = await Album.find({"published": true}).sort({year: 1});
                }
                res.send(album);
            } catch (e) {
                res.sendStatus(500);
            }
        } else {
            try {
                if (req.query.artist) {
                    album = await Album.find({"artist": req.query.artist}).sort({year: 1}).populate('artist');
                } else {
                    album = await Album.find().sort({year: 1});
                }
                res.send(album);
            } catch (e) {
                res.sendStatus(500);
            }
        }
    });
    router.get("/:id", async (req, res) => {
        try {
            let album = await Album.findById(req.params.id).populate('artist');
            res.send(album);
        } catch (e) {
            res.status(404).send('404 not found');
        }
    });
    router.post("/",auth, upload.single("image"), async (req, res) => {
        const albumData = new Album({
            title: req.body.title,
            image: req.body.image,
            artist: req.body.artist,
            year: req.body.year,
            user: req.user._id
        });
        if (req.file) {
            albumData.image = req.file.filename;
        }
        try {
            await albumData.save();
            if (req.user.role === "user") {
                res.send({message:"The data will be published after the administrator's consent"});
            }
            res.send(albumData);
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.put("/:id/publish",[auth, permit('admin')], async (req, res) => {
        req.body.user = req.user._id;
        let item = await Album.findById({_id: req.params.id});
        try {
            await item.set({...req.body, published: req.body.published});
            let newItem = await item.save();
            res.send(newItem);
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    });

    router.delete("/:id",[auth, permit('admin')], async (req, res) => {
        const item = await Album.findById(req.params.id);
        try {
            await item.delete();
            res.send("This album was successfully deleted");
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    });

    return router;
};

module.exports = createRouter;