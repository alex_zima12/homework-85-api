const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.get("/",auth, async (req, res) => {
        const user = req.user;
         if (user.role === "user") {
             try {
                 const artists = await Artist.find({"published": true});
                 res.send(artists);
             } catch (e) {
                 res.sendStatus(500);
             }
         } else {
             try {
                 const artists = await Artist.find();
                 res.send(artists);
             } catch (e) {
                 res.sendStatus(500);
             }
         }
    });

    router.post("/",auth, upload.single("image"), async (req, res) => {
        const artistData = new Artist({
            title: req.body.title,
            image: req.body.image,
            information: req.body.information,
            user: req.user._id
        });

        if (req.file) {
            artistData.image = req.file.filename;
        }
        try {
            await artistData.save();
            res.send(artistData);
        } catch (e) {
            res.sendStatus(400);
        }
    });

    router.put("/:id/publish",[auth, permit('admin')], async (req, res) => {
        req.body.user = req.user._id;
        let item = await Artist.findById({_id: req.params.id});
        try {
            await item.set({...req.body, published: req.body.published});
            let newItem = await item.save();
            res.send(newItem);
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    });

    router.delete("/:id",[auth, permit('admin')], async (req, res) => {
        const item = await Artist.findById(req.params.id);
        console.log(item);

        try {
            await item.delete();
            if (req.user.role === "user") {
                res.send({message:"The data will be published after the administrator's consent"});
            }
            res.send("This artist was successfully deleted");
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    });

    return router;
};

module.exports = createRouter;