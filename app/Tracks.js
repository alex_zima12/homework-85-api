const express = require('express');
const router = express.Router();
const Tracks = require("../models/Track");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {
    router.get("/", auth, async (req, res) => {
        const user = req.user;
        let track;
        if (user.role === "user") {

            try {
                if (req.query.album) {
                    track = await Tracks.find({"album": req.query.album}, {"published": true}).sort({trackNum: 1}).populate({
                        path: 'album',
                        populate: {path: 'artist'}
                    });
                } else {
                    track = await Tracks.find({"published": true}).sort({trackNum: 1}).populate({
                        path: 'album',
                        populate: {path: 'artist'}
                    });
                }
                res.send(track);
            } catch (e) {
                res.sendStatus(500);
            }
        } else {
            try {
                if (req.query.album) {
                    track = await Tracks.find({"album": req.query.album}).sort({trackNum: 1}).populate({
                        path: 'album',
                        populate: {path: 'artist'}
                    });
                } else {
                    track = await Tracks.find().sort({trackNum: 1}).populate({
                        path: 'album',
                        populate: {path: 'artist'}
                    });
                }
                res.send(track);
            } catch (e) {
                res.sendStatus(500);
            }
        }

    });
    router.post("/", auth, async (req, res) => {
        const trackData = new Tracks({
            title: req.body.title,
            album: req.body.album,
            trackNum: req.body.trackNum,
            duration: req.body.duration,
            user: req.user._id
        });
        try {
            await trackData.save();
            res.send(trackData);
        } catch (e) {
            console.log(e);
            res.sendStatus(400);
        }
    });

    router.put("/:id/publish", [auth, permit('admin')], async (req, res) => {
        req.body.user = req.user._id;
        let item = await Track.findById({_id: req.params.id});
        try {
            await item.set({...req.body, published: req.body.published});
            let newItem = await item.save();
            res.send(newItem);
        } catch (e) {
            console.log(e);
            res.status(500).send(e);
        }
    });

    router.delete("/:id", [auth, permit('admin')], async (req, res) => {
        const item = await Track.findById(req.params.id);
        try {
            await item.delete();
            if (req.user.role === "user") {
                res.send({message:"The data will be published after the administrator's consent"});
            }
            res.send("This track was successfully deleted");
        } catch (e) {
            console.log(e);
            res.status(500).send(e);
        }
    });


    return router;
};

module.exports = createRouter;